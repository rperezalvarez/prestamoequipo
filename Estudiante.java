
// Definición de la clase estudiante
public class Estudiante {
   // Definión de atributos globales de la clase
   private String nombre;
   private String carne;
   // Definición de método constructor de la clase
   public Estudiante(){
	 // Se asignan espacios vacios a cada atributo
     nombre = "";
     carne = "";
   }
   /* Método que recibe un parámetro y asignar este 
    este valor  al atributo nombre*/
   public void setNombre(String nombre){
	   this.nombre = nombre;
   } 
   // Método que retorna el valor del atributo nombre
   public String getNombre(){
	   return nombre;
	}
	
	public void nombreMetodo (int parametro1, String parametro2, int edad, char letra) {
		int conteoVocal = 0,conteoConsonante = 0;
		
		if ( edad > 20 ){
			edad = edad + 1;
		}
		else{
			edad -= 1; 
		}
		
		switch (letra){
			case 'a' :
				conteoVocal += 1;
				break;
			case 'e':
				conteoVocal += 1;
				break;
			case 'I':
				conteoVocal += 1;
				break;
			case 'o':
				conteoVocal += 1;
				break;
			case 'u': 
				conteoVocal += 1;
				break;
			default:
				conteoConsonante += 1;
		}
		
	}
} // cierre de la clase estudiante
