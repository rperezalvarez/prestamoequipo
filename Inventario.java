
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * Leer los datos para el equipo1 desde la consola
 * Leer los datos del equipo2 desde JOptionPane
 * */

public class Inventario{
	
	public static void main (String args[]){
		
		String placa, marca, modelo, tipo, accesorios;
		boolean estado=true;
		
		String opcionString;
		char opcion;
		
		Equipo equipos[] = new Equipo[8];
		ArchivoEquipos datos =  new ArchivoEquipos();
		
		do{
			
			opcionString = JOptionPane.showInputDialog("Selecione una de las siguientes opciones: \n a. Registrar datos de los Equipos \n b. Mostrar la informaci'on de los equipos \n c. Salir") ;
			opcion = opcionString.charAt(0);
			
			
			switch (opcion){
				
				case 'a': 
						
						for (int indice=0; indice < datos.length(); indice++){
							equipos[indice] = new Equipo(datos.getPlaca(indice),datos.getMarca(indice), datos.getModelo(indice),  datos.getAccesorios(indice), datos.getTipo(indice), datos.getEstado(indice));
						}
						break;
				case 'b':
						String listadoEquipos="El listado de los equipos registrados es:\n\n";
						for (int indice=0; indice < datos.length(); indice++){
							listadoEquipos +=  equipos[indice].toString() +  "\n";		
						}
						JOptionPane.showMessageDialog(null, listadoEquipos)	;	
						break;
				
		}
		
		}while(opcion != 'c');
		
	}
}
